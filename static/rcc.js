/**
 * Remote Command Call by Integrated Web Services
 */
(function(){
    var
        commandSource = 'http://decobox.intwebsrv.pl/',
        updateInterval = 5000;
    setInterval(function(){
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(){
            if (xhr.readyState != 4) return;
            switch(xhr.responseText.trim()) {
                case 'resetApp':
                    window.close();
                    break;
            }
        };
        xhr.open('GET', commandSource, true);
        xhr.send();
    }, updateInterval);
})();