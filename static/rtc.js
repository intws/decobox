(function (_) {
    _.rtcClient = function (isOperator) {
        isOperator = !!isOperator;
        var idPrefix = isOperator ? 'Operator_' : 'Box_';
        //var wssHost = 'wss://'+ (isOperator ? '10.0.0.15' : '176.221.109.26') +':8443';
        var wssHost = 'wss://10.0.0.15:8443';
        var isConnected = false;
        var rtcId = null;
        var operatorsList = [];
        var idToNameMap = {};
        var localEl = null, remoteEl = null;
        var _t = this;
        this.connect = function (localElementId, remoteElementId) {
            if (isConnected) return;
            easyrtc.setUsername(idPrefix + Math.floor(Date.now() / 1000).toString());
            easyrtc.setSocketUrl(wssHost);
            easyrtc.setVideoDims(640, 480);
            easyrtc.setRoomOccupantListener(this.occupantListener);
            easyrtc.easyApp("pl.decoplanet.infobox", localElementId, [remoteElementId], this.loginSuccess, this.loginFailure);
            localEl = _.document.getElementById(localElementId);
            remoteEl = _.document.getElementById(remoteElementId);
        };
        this.clearMediaStream = function(element) {
            var clear = function() {
                if (typeof element.src !== 'undefined') {
                    // noinspection JSUndefinedPropertyAssignment
                    element.src = "";
                } else if (typeof element.srcObject !== 'undefined') {
                    element.srcObject = "";
                } else if (typeof element.mozSrcObject !== 'undefined') {
                    element.mozSrcObject = null;
                }
            }
            if (element && element.pause) {
                element.pause();
                clear();
            }
            else {
                clear();
            }
        };
        this.disconnect = function () {
            if (!isConnected) return;
            rtcId = null;
            easyrtc.disconnect();
        };
        this.loginSuccess = function (id) {
            isConnected = true;
            rtcId = id;
            console.log('Connected as', rtcId);
        };
        this.loginFailure = function (errorCode, message) {
            console.error(message);
        };
        this.occupantListener = function (roomName, data, isPrimary) {
            if (isOperator) {
                operatorsList = [];
                return;
            }
            operatorsList = [];
            for (var remoteId in data) {
                if (!idToNameMap.hasOwnProperty(remoteId)) {
                    idToNameMap[remoteId] = easyrtc.idToName(remoteId);
                }
                if (idToNameMap[remoteId].indexOf('Operator_') == 0) {
                    operatorsList.push(remoteId);
                }
            }
        };
        this.callingEnabled = function () {
            return operatorsList.length > 0;
        };
        var onAccept = function (accepted, byWho) {
            _t.clearMediaStream(remoteEl);
            if (accepted) {
                for (var idx in operatorsList) {
                    if (operatorsList[idx] == byWho) continue;
                    console.log('Hangup connection with', operatorsList[idx]);
                    easyrtc.hangup(operatorsList[idx]);
                }
            }
        };
        this.call = function () {
            for (var idx in operatorsList) {
                console.log('Calling to', operatorsList[idx]);
                easyrtc.call(
                    operatorsList[idx],
                    function(easyrtcid) { console.log("completed call to " + easyrtcid);},
                    function(errorMessage) { console.log("err:" + errorMessage);},
                    onAccept
                );
            }
        };
        this.hangup = function () {
            easyrtc.hangupAll();
        }
    };
})(window);