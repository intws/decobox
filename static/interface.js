(function ($) {
    $(function () {
        var serviceMode = false;
        var screens = {
                left: [0, 0, 1366, 768],
                right: [1366, 0, 1280, 1024]
            }, communicationEnabled = false,
            localVideo = document.getElementById('localVideo'),
            remoteVideo = document.getElementById('remoteVideo');
        var rctConnection = new rtcClient(false);
        rctConnection.connect('localVideo', 'remoteVideo');
        var fn = {
            moveToLeftScreen: function (el) {
                el.css({
                    left: screens.left[0],
                    top: screens.left[1],
                    width: screens.left[2],
                    height: screens.left[3]
                });
            },
            moveToRightScreen: function (el) {
                el.css({
                    left: screens.right[0],
                    top: screens.right[1],
                    width: screens.right[2],
                    height: screens.right[3]
                });
            },
            hide: function (el) {
                el.css({
                    opacity: 0,
                    zIndex: 1
                });
            },
            show: function (el, mode) {
                if (mode == undefined) mode = 'css';
                el[mode]({
                    opacity: 1,
                    zIndex: 2
                });
            }
        };
        var
            deco = $('#website'),
            decoFrame = $('#websiteFrame'),
            appMenu = $('#app-menu'),
            keyboard = $('#keyboard'),
            slider = $('#slider'),
            skype = $('#skype'),
            skypeControl = $('#skypeControl'),
            skypeFrame = $('#skypeFrame').hide(),
            aboutUs = $('#about-us'),
            contact = $('#contact'),
            getPhone = $('#get-phone');
        // prepare interface after start
        fn.hide(skype);
        fn.hide(deco);
        fn.hide(keyboard);
        fn.hide(skypeControl);
        fn.hide(contact);
        fn.hide(aboutUs);
        fn.hide(getPhone);
        fn.moveToLeftScreen(appMenu);
        fn.moveToRightScreen(slider);
        fn.show(appMenu);
        fn.show(slider);
        var activeElement;
        if (serviceMode) {
            document.getElementById('serviceModeInfo').style.display = 'block';
        }
        $('#keyboard span[data-key]').on('click', function () {
            if ($(this).data('key') == 'backspace') {
                activeElement.val(activeElement.val().substr(0, activeElement.val().length - 1))
            } else
            activeElement.val(activeElement.val() + $(this).data('key'));
            if (activeElement.val() == 'resetappnow') {
                window.close();
            }
        });
        var getPhoneForm = $('#get-phone-form'), getPhoneFormInput = getPhoneForm.find('input'), getPhoneMessage = $('#get-phone-message'), phoneInputVisual = $('#phone-input');
        getPhone.find('span[data-key]').on('click', function(){
            getPhoneFormInput.val( getPhoneFormInput.val() + $(this).data('key').toString() );
            phoneInputVisual.text(getPhoneFormInput.val());
        });
        getPhoneForm.find('button').on('click', function () {
            $.ajax({
                url: 'http://decobox.intwebsrv.pl/phone.php',
                data: {
                    no: getPhoneFormInput.val()
                },
                type: 'POST',
                success: function(r) {
                    getPhoneFormInput.val('');
                    phoneInputVisual.text('');
                    getPhoneMessage.text(r.message).css('display', 'block');
                    fn.hide(skype);
                    fn.hide(deco);
                    fn.hide(keyboard);
                    fn.hide(skypeControl);
                    fn.hide(contact);
                    fn.hide(aboutUs);
                    fn.moveToLeftScreen(appMenu);
                    fn.moveToRightScreen(slider);
                    fn.show(appMenu);
                    fn.show(slider);
                }
            });
        });
        decoFrame.on('load', function () {
            var iframe = decoFrame.contents().find('html');
            iframe.find('input').on('focus', function (e) {
                activeElement = $(e.target);
                fn.moveToRightScreen(deco);
                fn.moveToLeftScreen(keyboard);
                fn.show(keyboard);
                fn.hide(slider);
            });
        });
        $('#back-to-website').on('click', function () {
            if (serviceMode)return;
            fn.moveToLeftScreen(deco);
            fn.moveToRightScreen(slider);
            fn.hide(keyboard);
            fn.show(slider);
        });
        $('.back-to-home').click(function () {
            if (serviceMode)return;
            fn.hide(skype);
            fn.hide(deco);
            fn.hide(keyboard);
            fn.hide(skypeControl);
            fn.hide(contact);
            fn.hide(aboutUs);
            fn.hide(getPhone);
            fn.moveToLeftScreen(appMenu);
            fn.moveToRightScreen(slider);
            fn.show(appMenu);
            fn.show(slider);
        });
        $('.end-call').click(function () {
            if (serviceMode)return;
            communicationEnabled = false;
            //stopConnection();
            //disconnect();
            rctConnection.hangup();
            fn.hide(skype);
            fn.hide(deco);
            fn.hide(keyboard);
            fn.hide(skypeControl);
            fn.moveToLeftScreen(appMenu);
            fn.moveToRightScreen(slider);
            fn.show(appMenu);
            fn.show(slider);
        });
        $('#show-about').click(function () {
            if (serviceMode)return;
            fn.moveToLeftScreen(aboutUs);
            fn.show(aboutUs);
            fn.hide(appMenu);
        });
        $('#show-contact').click(function () {
            if (serviceMode)return;
            fn.moveToLeftScreen(contact);
            fn.show(contact);
            fn.hide(appMenu);
        });
        $('#show-deco').click(function () {
            if (serviceMode)return;
            decoFrame.attr('src', 'http://decoplanet.pl/');
            fn.moveToLeftScreen(deco);
            fn.show(deco);
            fn.hide(appMenu);
        });
        // skype support
        $('#make-skype-call').click(function () {
            if (serviceMode)return;
            if (!rctConnection.callingEnabled()) {
                getPhoneFormInput.val('');
                phoneInputVisual.text('');
                getPhoneMessage.text('').css('display', 'none');
                fn.show(getPhone);
                fn.moveToRightScreen(slider);
                fn.moveToLeftScreen(getPhone);
                fn.hide(appMenu);
                return;
            }
            communicationEnabled = true;
            rctConnection.call();
            setTimeout(function () {
                fn.show(skype);
                fn.moveToRightScreen(skype);
                fn.show(skypeControl);
                fn.moveToLeftScreen(skypeControl);
                fn.hide(appMenu);
                fn.hide(slider);
                //
            }, 1000);
        });
    });
})(window.jQuery);